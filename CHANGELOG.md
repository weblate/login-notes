# Changelog

## 0.4.0 - 2021-03-01
### Added
* Support for Nextcloud 21 and 22
* Support for PHP 8.0

### Removed
* Drop support for Nextcloud 17 and Nextcloud 18

### Internal
* Improved CI, lint, tests and static analysis

### Translations
* German (new!)

## 0.3.0 - 2020-08-28
### Added
* Support for Nextcloud 20

### Fixed
* Release with new certificate (fixes #4)

### Changed
* Bump dependencies

### Removed
* Drop support for Nextcloud 16.

## 0.2.1 - 2020-04-19
### Fixed
* Fixed an issue between app's and Nextcloud's incompatible Symfony Event Dispatcher version

## 0.2.0 - 2020-04-19
### Added
* Option to pick between centered text and aligned notes
* CI for NodeJS & PHP lint checks

### Changed
* Handle correct text direction for RTL languages

## 0.1.0 - 2020-04-15
* Initial release.
