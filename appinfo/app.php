<?php
/**
 * @copyright Copyright (c) 2020 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\LoginNotes\AppInfo;

use OCP\IInitialStateService;
use OCA\LoginNotes\Manager;
use OCP\Util;

@include_once __DIR__ . '/../vendor/autoload.php';

const APP_NAME = 'login_notes';

$request = \OC::$server->getRequest();

if (isset($request->server['REQUEST_URI'])) {
	$url = $request->server['REQUEST_URI'];
	if (preg_match('%/login(\?.+)?$%m', $url)) {
		$notes = \OC::$server->query(Manager::class)->getNotes();
		$isCentered = \OC::$server->query('OCP\IConfig')->getAppValue(APP_NAME, 'centered', false);
		/** @var IInitialStateService $initialStateService */
		$initialStateService = \OC::$server->query('OCP\IInitialStateService');
		$initialStateService->provideInitialState(APP_NAME, 'centered', $isCentered);
		$initialStateService->provideInitialState(
				'login_notes',
				'notes',
				$notes
			);
		Util::addScript(APP_NAME, 'login_notes-main');
	}
}
