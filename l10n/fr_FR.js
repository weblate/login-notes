OC.L10N.register(
    "login_notes",
    {
    "Login notes" : "Notes de connexion",
    "Display notes on log-in page" : "Afficher des notes sur la page de connexion",
    "This app will show admin defined notes the log-in page" : "Cette application affichera des notes définies par l'administrateur⋅ice sur la page de connexion",
    "Enter the text for the updated login note here. Markdown supported." : "Entrez le texte pour votre nouvelle note sur la page de connexion. Markdown supporté.",
    "Update note" : "Mettre à jour la note",
    "Cancel" : "Annuler",
    "Edit" : "Éditer",
    "Delete" : "Supprimer",
    "Notes on login screen" : "Notes sur la page de connexion",
    "Text alignment" : "Alignement du texte",
    "Aligned" : "Aligné",
    "Note content will be aligned to left (or right with RTL languages)." : "Le contenu des notes sera aligné à gauche (ou à droite dans le cas de systèmes d'écriture sinistroverses).",
    "Centered" : "Centré",
    "Note content will be centered." : "Le contenu des notes sera centré.",
    "Notes list" : "Liste de notes",
    "These notes will be shown on the login screen." : "Ces notes seront affichés sur la page de connexion.",
    "No notes yet" : "Pas encore de notes",
    "New login note" : "Nouvelle note",
    "Enter the text for your new login note here. Markdown supported." : "Entrez le texte pour votre nouvelle note sur la page de connexion. Markdown supporté.",
    "Add new note" : "Ajouter une nouvelle note"
},
"nplurals=2; plural=(n > 1);");
