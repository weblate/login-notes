<?php
/**
 * @copyright 2020 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\LoginNotes\Settings;

use OCA\LoginNotes\Manager;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\IInitialStateService;
use OCP\Settings\ISettings;

class AdminSettings implements ISettings {

	/** @var Manager */
	private $manager;

	/** @var IInitialStateService */
	private $initialStateService;

	/** @var IConfig */
	private $config;

	/**
	 * AdminSettings constructor.
	 *
	 * @param Manager $manager
	 * @param IInitialStateService $initialStateService
	 * @param IConfig $config
	 */
	public function __construct(Manager $manager, IInitialStateService $initialStateService, IConfig $config) {
		$this->manager = $manager;
		$this->initialStateService = $initialStateService;
		$this->config = $config;
	}

	/**
	 * @return TemplateResponse
	 */
	public function getForm() {
		$isCentered = $this->config->getAppValue('login_notes', 'centered', 'false');
		$this->initialStateService->provideInitialState('login_notes', 'centered', $isCentered === 'true');
		$this->initialStateService->provideInitialState('login_notes', 'notes', $this->manager->getNotes());

		return new TemplateResponse('login_notes', 'admin');
	}

	/**
	 * @return string
	 */
	public function getSection() {
		return 'additional';
	}

	/**
	 * @return int
	 */
	public function getPriority() {
		return 10;
	}
}
