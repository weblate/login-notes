<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Controller;

use OCA\LoginNotes\Controller\NoteController;
use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;
use const OCA\LoginNotes\AppInfo\APP_NAME;

class NoteControllerTest extends TestCase {
	/**
	 * @var NoteController
	 */
	private $controller;
	/**
	 * @var Manager|MockObject
	 */
	private $manager;

	public function setUp(): void {
		parent::setUp();

		$request = $this->createMock(IRequest::class);
		$this->manager = $this->createMock(Manager::class);
		$this->controller = new NoteController(APP_NAME, $request, $this->manager);
	}

	public function testCreateNote(): void {
		$note = $this->createMock(Note::class);
		$this->manager->expects(self::once())->method('create')->with('new note')->willReturn($note);
		$response = new DataResponse($note);
		self::assertEquals($response, $this->controller->create('new note'));
	}

	public function testUpdateNote(): void {
		$note = $this->createMock(Note::class);
		$this->manager->expects(self::once())->method('update')->with(1, 'updated note')->willReturn($note);
		$response = new DataResponse($note);
		self::assertEquals($response, $this->controller->update(1, 'updated note'));
	}

	public function testUpdateNoteWithNotExistingNote(): void {
		$this->manager->expects(self::once())->method('update')->with(1, 'updated note')->willThrowException(new DoesNotExistException(''));
		$response = new DataResponse([], Http::STATUS_NOT_FOUND);
		self::assertEquals($response, $this->controller->update(1, 'updated note'));
	}

	public function testUpdateNoteWithMultipleObjectReturned(): void {
		$this->manager->expects(self::once())->method('update')->with(1, 'updated note')->willThrowException(new MultipleObjectsReturnedException(''));
		$response = new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		self::assertEquals($response, $this->controller->update(1, 'updated note'));
	}

	public function testDeleteNote(): void {
		$note = $this->createMock(Note::class);
		$this->manager->expects(self::once())->method('getById')->with(1)->willReturn($note);
		$this->manager->expects(self::once())->method('delete')->with($note);
		$response = new DataResponse($note);
		self::assertEquals($response, $this->controller->destroy(1));
	}

	public function testDeleteNoteWithNotExistingNote(): void {
		$this->manager->expects(self::once())->method('getById')->with(1)->willThrowException(new DoesNotExistException(''));
		$response = new DataResponse([], Http::STATUS_NOT_FOUND);
		self::assertEquals($response, $this->controller->destroy(1));
	}
}
