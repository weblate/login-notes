<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Controller;

use OCA\LoginNotes\Controller\SettingController;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\IConfig;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;
use const OCA\LoginNotes\AppInfo\APP_NAME;

class SettingsControllerTest extends TestCase {
	/**
	 * @var SettingController
	 */
	private $controller;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	public function setUp(): void {
		parent::setUp();

		$request = $this->createMock(IRequest::class);
		$this->config = $this->createMock(IConfig::class);
		$this->controller = new SettingController(APP_NAME, $request, $this->config);
	}

	public function setAppValueDataProvider(): array {
		return [
			[true],
			[false]
		];
	}

	/**
	 * @dataProvider setAppValueDataProvider
	 * @param bool $centered
	 */
	public function testSetAppValue(bool $centered): void {
		$this->config->expects(self::once())->method('setAppValue')->with('login_notes', 'centered', (string) $centered);
		$response = new DataResponse([], Http::STATUS_OK);
		self::assertEquals($response, $this->controller->set($centered));
	}
}
