<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests;

use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Model\NoteMapper;
use OCP\AppFramework\Utility\ITimeFactory;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class ManagerTest extends TestCase {
	/**
	 * @var Manager
	 */
	private $manager;
	/**
	 * @var NoteMapper|MockObject
	 */
	private $noteMapper;
	/**
	 * @var ITimeFactory|MockObject
	 */
	private $timeFactory;

	public function setUp(): void {
		parent::setUp();

		$this->timeFactory = $this->createMock(ITimeFactory::class);
		$this->noteMapper = $this->createMock(NoteMapper::class);
		$this->manager = new Manager($this->noteMapper, $this->timeFactory);
	}


	public function testGetById(): void {
		$note = $this->createMock(Note::class);
		$this->noteMapper->expects(self::once())->method('getById')->with(5)->willReturn($note);
		self::assertEquals($note, $this->manager->getById(5));
	}

	public function testCreate(): void {
		$this->timeFactory->expects(self::once())->method('getTime')->willReturn(60);
		$note = new Note();
		$note->setRawText('my **note**');
		$note->setText("<p>my <strong>note</strong></p>\n");
		$note->setCreatedAt(60);
		$this->noteMapper->expects(self::once())->method('insert')->with($note)->willReturn($note);
		$insertedNote = $this->manager->create(' my **note** ');
		self::assertEquals($note->getText(), $insertedNote->getText());
		$this->manager->delete($insertedNote);
	}

	public function testUpdate(): void {
		$note = new Note();
		$note->setRawText('my **note**');
		$note->setText("<p>my <strong>note</strong></p>\n");
		$note->setCreatedAt(60);
		$this->noteMapper->expects(self::once())->method('getById')->with(5892)->willReturn($note);
		$updatedNote = clone $note;
		$updatedNote->setText("<p>my <strong>updated note</strong></p>\n");
		$updatedNote->setRawText('my **updated note**');
		$this->noteMapper->expects(self::once())->method('update')->with($note)->willReturn($updatedNote);
		$updatedInsertedNote = $this->manager->update(5892, ' my **updated note** ');
		self::assertEquals($updatedNote->getText(), $updatedInsertedNote->getText());
		$this->manager->delete($updatedInsertedNote);
	}

	public function testDelete(): void {
		$note = new Note();
		$note->setRawText('my **note**');
		$note->setText("<p>my <strong>note</strong></p>\n");
		$note->setCreatedAt(60);
		$this->noteMapper->expects(self::once())->method('delete')->with($note)->willReturn($note);
		self::assertEquals($note, $this->manager->delete($note));
	}

	public function testGetNotes(): void {
		$note = new Note();
		$note->setRawText('my **note**');
		$note->setText("<p>my <strong>note</strong></p>\n");
		$note->setCreatedAt(60);
		$this->noteMapper->expects(self::once())->method('getNotes')->with(null, null)->willReturn([$note]);
		self::assertEquals([$note], $this->manager->getNotes());
	}
}
