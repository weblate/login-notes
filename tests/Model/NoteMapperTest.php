<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Model;

use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Model\NoteMapper;
use OCP\AppFramework\Db\DoesNotExistException;
use ChristophWurst\Nextcloud\Testing\TestCase;

class NoteMapperTest extends TestCase {
	/**
	 * @var NoteMapper
	 */
	private $noteMapper;

	public function setUp(): void {
		parent::setUp();

		$this->noteMapper = new NoteMapper(\OC::$server->getDatabaseConnection());
	}

	public function testGetById(): void {
		$note = $this->getNote();
		$insertedNote = $this->noteMapper->insert($note);
		$note->resetUpdatedFields();
		$result = $this->noteMapper->getById($insertedNote->getId());
		self::assertEquals($note, $result);
		$this->noteMapper->delete($insertedNote);
	}

	public function testGetByIdWithNotFound(): void {
		$this->expectException(DoesNotExistException::class);
		$this->noteMapper->getById(5928);
	}

	public function testDelete(): void {
		$note = $this->getNote();
		$insertedNote = $this->noteMapper->insert($note);
		$this->noteMapper->delete($insertedNote);
		$this->expectException(DoesNotExistException::class);
		$this->noteMapper->getById($insertedNote->getId());
	}

	public function testGetNotes(): void {
		$note = $this->getNote();
		$note2 = $this->getNote('note2', 'note2', 15);
		$insertedNote = $this->noteMapper->insert($note);
		$insertedNote2 = $this->noteMapper->insert($note2);
		$insertedNote->resetUpdatedFields();
		$insertedNote2->resetUpdatedFields();

		self::assertEquals([$insertedNote2, $insertedNote], $this->noteMapper->getNotes());

		self::assertEquals([$insertedNote2], $this->noteMapper->getNotes(1));

		self::assertEquals([$insertedNote], $this->noteMapper->getNotes(1, 1));

		$this->noteMapper->delete($insertedNote);
		$this->noteMapper->delete($insertedNote2);
	}

	private function getNote(string $text = 'toto', string $rawText = 'toto', int $createdAt = 0): Note {
		$note = new Note();
		$note->setText($text);
		$note->setRawText($rawText);
		$note->setCreatedAt($createdAt);
		return $note;
	}
}
