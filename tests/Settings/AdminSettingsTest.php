<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Settings;

use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Settings\AdminSettings;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use ChristophWurst\Nextcloud\Testing\TestCase;
use OCP\IInitialStateService;
use PHPUnit\Framework\MockObject\MockObject;

class AdminSettingsTest extends TestCase {
	/**
	 * @var Manager|MockObject
	 */
	private $manager;
	/**
	 * @var IInitialStateService|MockObject
	 */
	private $initialStateService;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;
	/**
	 * @var AdminSettings
	 */
	private $adminSettings;

	public function setUp(): void {
		parent::setUp();
		$this->manager = $this->createMock(Manager::class);
		$this->initialStateService = $this->createMock(IInitialStateService::class);
		$this->config = $this->createMock(IConfig::class);
		$this->adminSettings = new AdminSettings($this->manager, $this->initialStateService, $this->config);
	}

	public function testGetPriority() : void {
		self::assertEquals(10, $this->adminSettings->getPriority());
	}

	public function testGetSection() : void {
		self::assertEquals('additional', $this->adminSettings->getSection());
	}

	public function dataForTestGetForm(): array {
		return [
			['true', true, [new Note()]],
			['false', false, [new Note()]],
		];
	}

	/**
	 * @dataProvider dataForTestGetForm
	 * @param string $centered
	 * @param bool $centeredBool
	 * @param array $notes
	 */
	public function testGetForm(string $centered, bool $centeredBool, array $notes): void {
		$this->config->expects(self::once())->method('getAppValue')->with('login_notes', 'centered', 'false')->willReturn($centered);
		$this->initialStateService->expects(self::exactly(2))->method('provideInitialState')->withConsecutive(['login_notes', 'centered', $centeredBool], ['login_notes', 'notes', $notes]);
		$this->manager->expects(self::once())->method('getNotes')->with()->willReturn($notes);
		$response = new TemplateResponse('login_notes', 'admin');
		self::assertEquals($response, $this->adminSettings->getForm());
	}
}
