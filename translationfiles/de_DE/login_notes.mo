��          �      l      �     �     �     �     �               *  C   /  @   s     �     �     �  C   �        
   ?     J     `  .   o  6   �     �  O  �     1     M  	   Z  	   d     n  ,   w  
   �  c   �  Z        n     }     �  W   �  .   	     8     Q     o  >     P   �                                           	                           
                                 Add new note Aligned Cancel Centered Delete Display notes on log-in page Edit Enter the text for the updated login note here. Markdown supported. Enter the text for your new login note here. Markdown supported. Login notes New login note No notes yet Note content will be aligned to left (or right with RTL languages). Note content will be centered. Notes list Notes on login screen Text alignment These notes will be shown on the login screen. This app will show admin defined notes the log-in page Update note Project-Id-Version: Nextcloud 3.14159
Report-Msgid-Bugs-To: translations\@example.com
PO-Revision-Date: 2021-02-28 12:38+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
 Neuen Login-Hinweis anlegen Linksbündig Abbrechen Zentriert Löschen Zeigt einen Hinweis auf dem Login-Bereich an Bearbeiten Geben Sie hier den Text für den aktualisierten Login-Hinweis hier ein. Markdown wird unterstützt. Geben Sie hier den Text für den neuen Login-Hinweis hier ein. Markdown wird unterstützt. Login-Hinweise Neuer Login-Hinweis Keine Login-Hinweise vorhanden. Der Login-Hinweis wird linksbündig ausgerichtet (oder rechtsbündig bei RTL-Sprachen). Der Login-Hinweis wird zentriert ausgerichtet. Liste der Login-Hinweise Hinweis auf dem Login-Bereich Textausrichtung Diese Hinweise werden werden auf dem Login-Bereich ausgegeben. Diese App zeigt vom Administrator hinterlegte Hinweise auf dem Login-Bereich an. Login-Hinweis aktualisieren 