��          �      l      �     �     �     �     �               *  C   /  @   s     �     �     �  C   �        
   ?     J     `  .   o  6   �     �  N  �     0     J     R     Z  	   b  +   l     �  V   �  V   �     N     a     o  o   �  "   �          %     D  4   X  c   �     �                                      	                           
                                 Add new note Aligned Cancel Centered Delete Display notes on log-in page Edit Enter the text for the updated login note here. Markdown supported. Enter the text for your new login note here. Markdown supported. Login notes New login note No notes yet Note content will be aligned to left (or right with RTL languages). Note content will be centered. Notes list Notes on login screen Text alignment These notes will be shown on the login screen. This app will show admin defined notes the log-in page Update note Project-Id-Version: Nextcloud 3.14159
Report-Msgid-Bugs-To: translations\@example.com
PO-Revision-Date: 2021-02-28 12:35+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 Ajouter une nouvelle note Aligné Annuler Centré Supprimer Afficher des notes sur la page de connexion Éditer Entrez le texte pour votre nouvelle note sur la page de connexion. Markdown supporté. Entrez le texte pour votre nouvelle note sur la page de connexion. Markdown supporté. Notes de connexion Nouvelle note Pas encore de notes Le contenu des notes sera aligné à gauche (ou à droite dans le cas de systèmes d'écriture sinistroverses). Le contenu des notes sera centré. Liste de notes Notes sur la page de connexion Alignement du texte Ces notes seront affichés sur la page de connexion. Cette application affichera des notes définies par l'administrateur⋅ice sur la page de connexion Mettre à jour la note 