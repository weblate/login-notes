const webpackConfig = require('@nextcloud/webpack-vue-config')
const path = require('path')

module.exports = {
	...webpackConfig,
	entry: {
		main: path.join(__dirname, 'src/main.js'),
		settings: path.join(__dirname, 'src/Settings.js'),
	},
}
